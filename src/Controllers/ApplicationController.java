package Controllers;

import Entities.Client;
import Entities.ClientEventEnum;
import Entities.Mail;
import Services.ClientService;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

public class ApplicationController {

    private Client client;
    private ClientService clientService;

    public ApplicationController(Client client, ClientService clientService) {
        this.client = client;
        this.clientService = clientService;
    }

    public void formIP(Stage stage) {
        // IP form
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Scene scene = new Scene(grid, 400, 400);

        Text scenetitle = new Text("Adresse IP");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        Label ipAdresseLabel = new Label("IP :");
        grid.add(ipAdresseLabel, 0, 1);

        TextField ipTextField = new TextField();
        ipTextField.setText("127.0.0.1");
        grid.add(ipTextField, 1, 1);

        Label portLabel = new Label("port :");
        grid.add(portLabel, 0, 2);

        TextField portTextField = new TextField();
        portTextField.setText("5000");
        grid.add(portTextField, 1, 2);

        Button btn = new Button("Connexion");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_CENTER);
        hbBtn.getChildren().add(btn);
        grid.add(hbBtn, 1, 4);

        Text text = new Text();
        text.setFill(Color.FIREBRICK);
        grid.add(text, 1, 6);

        btn.setOnAction(e -> {
            text.setText("");
            if (ipTextField.getText().isEmpty() || portTextField.getText().isEmpty()) {
                text.setText("Fill the form in correctly please");
            } else {
                String[] infos = {ipTextField.getText(), portTextField.getText()};
                if (!this.clientService.actionOnEvent(ClientEventEnum.CONNEXION_SERVEUR, infos)) {
                    text.setText("Can't connect to POP3 server");
                } else {
                    this.formUser(stage);
                }
            }
        });

        stage.setScene(scene);
    }

    public void formUser(Stage stage) {
        // IP form
        AtomicInteger nbrEssaisRestant = new AtomicInteger(3);

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Scene scene = new Scene(grid, 400, 400);

        Button retour = new Button("Retour");
        retour.setOnAction(e -> {
            this.clientService.actionOnEvent(ClientEventEnum.QUIT, null);
            this.formIP(stage);
        });

        HBox hbretour = new HBox(10);
        hbretour.setAlignment(Pos.TOP_LEFT);
        hbretour.getChildren().add(retour);
        grid.add(hbretour, 0, 0);

        Text scenetitle = new Text("Utilisateur");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 1, 2, 1);

        Label nomLabel = new Label("Nom utilisateur :");
        grid.add(nomLabel, 0, 2);

        TextField nomTextField = new TextField();
        nomTextField.setText("merlot");
        grid.add(nomTextField, 1, 2);

        Label passLabel = new Label("Mot de passe :");
        grid.add(passLabel, 0, 3);

        PasswordField passwordField = new PasswordField();
        passwordField.setText("secret");
        grid.add(passwordField, 1, 3);

        Button btn = new Button("Connexion");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_CENTER);
        hbBtn.getChildren().add(btn);
        grid.add(hbBtn, 1, 4);

        Text text = new Text();
        text.setFill(Color.FIREBRICK);
        grid.add(text, 0, 5, 2, 1);

        btn.setOnAction(e -> {
            text.setText("");
            this.client.setName(nomTextField.getText());
            this.client.setPassword(passwordField.getText());
            if (nomTextField.getText().isEmpty() || passwordField.getText().isEmpty()) {
                text.setText("Les 2 champs doivent être remplis.");
            } else if (!this.clientService.actionOnEvent(ClientEventEnum.CONNEXION_UTILISATEUR, null)) {
                nbrEssaisRestant.getAndDecrement();
                if (nbrEssaisRestant.get() == 0) {
                    this.clientService.actionOnEvent(ClientEventEnum.QUIT, null);
                    this.formIP(stage);
                }else {
                    text.setText("Couple nom/mot de passe incorrecte. Il vous reste " + nbrEssaisRestant.get() + " essais.");
                }
            } else {
                this.messagerie(stage);
            }
        });

        stage.setScene(scene);
    }

    public void messagerie(Stage stage) {
        // IP form
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Scene scene = new Scene(grid, 750, 500);

        Text scenetitle = new Text("Bonjour " + this.client.getName());
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 4, 1);

        /*Button deconnexion = new Button("Déconnexion");
        deconnexion.setOnAction(e -> {
            if (this.client.getSocket() != null)
                this.clientService.actionOnEvent(ClientEventEnum.QUIT, null);

            this.formIP(stage);
        });

        HBox hBoxDeconnexion = new HBox(10);
        hBoxDeconnexion.setAlignment(Pos.TOP_LEFT);
        hBoxDeconnexion.getChildren().add(deconnexion);
        grid.add(hBoxDeconnexion, 0, 0);

        Button rafraichir = new Button("Rafraichir");
        rafraichir.setOnAction(e -> {
            this.clientService.actionOnEvent(ClientEventEnum.LIST, null);
        });

        HBox hBoxRafraichir = new HBox(10);
        hBoxRafraichir.setAlignment(Pos.TOP_RIGHT);
        hBoxRafraichir.getChildren().add(rafraichir);
        grid.add(hBoxRafraichir, 1, 0);*/

        TableView<Mail> table = new TableView<>();

        table.setEditable(false);

        TableColumn<Mail, String> from = new TableColumn<>("De");
        from.setCellValueFactory(new PropertyValueFactory<>("auteur"));

        TableColumn<Mail, String> object = new TableColumn<>("Sujet");
        object.setCellValueFactory(new PropertyValueFactory<>("sujet"));

        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        table.getColumns().addAll(Arrays.asList(from, object));

        table.setItems(this.client.getMailObservableList());

        grid.add(table, 0, 1, 2, 7);



        Text title = new Text("Message");
        title.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(title, 2, 1, 2, 1);

        Text messageFrom = new Text("From : ");
        grid.add(messageFrom, 2, 2);

        TextField textFieldFrom = new TextField();
        textFieldFrom.setEditable(false);
        grid.add(textFieldFrom, 3, 2);

        Text messageTo = new Text("To : ");
        grid.add(messageTo, 2, 3);

        TextField textFieldTo = new TextField();
        textFieldTo.setEditable(false);
        grid.add(textFieldTo, 3, 3);

        Text messageDate = new Text("Date : ");
        grid.add(messageDate, 2, 4);

        TextField textFieldDate = new TextField();
        textFieldDate.setEditable(false);
        grid.add(textFieldDate, 3, 4);

        Text messageSubject = new Text("Subject : ");
        grid.add(messageSubject, 2, 5);

        TextField textFieldSubject = new TextField();
        textFieldSubject.setEditable(false);
        grid.add(textFieldSubject, 3, 5, 2, 1);

        TextArea textFieldMessage = new TextArea();
        textFieldMessage.setEditable(false);
        grid.add(textFieldMessage, 3, 6, 2, 1);

        table.setRowFactory( tv -> {
            TableRow<Mail> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty()) {
                    Mail rowData = row.getItem();
                    textFieldFrom.setText(rowData.getAuteur());
                    textFieldSubject.setText(rowData.getSujet());
                    textFieldMessage.setText(rowData.getMessage());
                    textFieldDate.setText(rowData.getDate().toString());
                    textFieldTo.setText(rowData.getDestinataire());
                }
            });
            return row ;
        });

        GridPane.setVgrow(table, Priority.ALWAYS);
        GridPane.setHgrow(table, Priority.ALWAYS);

        stage.setScene(scene);

        this.clientService.actionOnEvent(ClientEventEnum.LIST, null);
        this.clientService.actionOnEvent(ClientEventEnum.QUIT, null);
    }
}
