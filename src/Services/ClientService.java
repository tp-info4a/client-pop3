package Services;

import Entities.Client;
import Entities.ClientEventEnum;
import Entities.Mail;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ClientService {
    private final static Logger log = Logger.getLogger(ClientService.class.getName());

    private Client client;

    public ClientService(Client client) {
        this.client = client;
    }

    /**
     *
     * @param clientEvent event
     * @param payload payload
     *  - CONNEXION_SERVEUR : {ip, port}
     *  - CONNEXION_UTILISATEUR : {}
     *  - QUIT : null
     *  - LIST : null
     *  - RETR : {messageId}
     * @return booléen
     */
    public boolean actionOnEvent(ClientEventEnum clientEvent, String[] payload) {
        switch (clientEvent) {
            case CONNEXION_SERVEUR:
                return connexionServer(payload[0], Integer.parseInt(payload[1]));

            case CONNEXION_UTILISATEUR:
                return authentication();

            case QUIT:
                quit();
                break;

            case LIST:
                List<String> list = list();
                if (list != null)
                    list.forEach(this::retr);
                break;

            default:
                return false;
        }

        return true;
    }

    private boolean connexionServer(String ip, int port) {
        try {
            this.client.setSocket(ip, port);
            String response = this.client.getBufferedReader().readLine();

            // log.info("reponse : " + response);
            String[] connexionInfos = response.split(" ");

            if (!connexionInfos[0].equals("+OK"))
                throw new Exception("Connexion not found");

            this.client.setSalt(connexionInfos[connexionInfos.length-1]);

            return true;
        } catch (Exception e) {
            // log.severe("Erreur lors de la connexion");
            e.printStackTrace();
            this.client.closeSocket();
            return false;
        }
    }

    /**
     * APOP function
     * @return client connecté ou non
     */
    private boolean authentication() {
        this.client.getMailObservableList().clear();
        byte[] passwordBytes = (this.client.getSalt() + this.client.getPassword()).getBytes(StandardCharsets.UTF_8);
        byte[] hash = this.client.getMessageDigest().digest(passwordBytes);

        String mess = "APOP " + this.client.getName() + " " + ApplicationService.byteToHex(hash) + Client.END_LINE;

        try {
            this.client.getBufferedOutputStream().write(mess.getBytes(StandardCharsets.UTF_8));
            this.client.getBufferedOutputStream().flush();

            String response = this.client.getBufferedReader().readLine();
            // log.info("reponse : " + response);

            return response.split(" ")[0].equals("+OK");
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void quit() {
        String mess = "QUIT" + Client.END_LINE;

        try {
            this.client.getBufferedOutputStream().write(mess.getBytes(StandardCharsets.UTF_8));
            this.client.getBufferedOutputStream().flush();
            // log.info("QUIT request has been sent");
            String response = this.client.getBufferedReader().readLine();
            // log.info("Réponse : " + response);
            this.client.closeSocket();
            // log.info("La connexion est fermée.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<String> list() {
        String mess = "LIST" + Client.END_LINE;

        try {
            this.client.getBufferedOutputStream().write(mess.getBytes(StandardCharsets.UTF_8));
            this.client.getBufferedOutputStream().flush();
            // log.info("LIST request has been sent");
            String reponse;
            String[] split;
            List<String> mailIds = new ArrayList<>();
            do {
                reponse = this.client.getBufferedReader().readLine();
                // log.info("Réponse : " + reponse);
                split = reponse.split(" ");

                if (!(split[0].equals("+OK") || split[0].equals("."))) {
                    mailIds.add(split[0]);
                }
            } while (!(reponse.equals(".") || split[0].equals("-ERR")));

            return mailIds;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void retr(String messageId) {
        String mess = "RETR " + messageId + Client.END_LINE;

        try {
            this.client.getBufferedOutputStream().write(mess.getBytes(StandardCharsets.UTF_8));
            this.client.getBufferedOutputStream().flush();
            // log.info("RETR request has been sent");
            String reponse;
            String[] split;
            Mail mail = new Mail();
            StringBuilder value = new StringBuilder();
            do {
                reponse = this.client.getBufferedReader().readLine();
                // log.info("Réponse : " + reponse);
                if (!(reponse.startsWith("+OK") || reponse.equals(".") || reponse.isEmpty())) {
                    split = reponse.split(":");
                    switch (split[0]) {
                        case "from":
                            value.setLength(0);
                            value = new StringBuilder();
                            for (int i = 1 ; i < split.length ; i++) {
                                value.append(split[i]);
                            }
                            mail.setAuteur(value.toString());
                            break;

                        case "to":
                            value.setLength(0);
                            value = new StringBuilder();
                            for (int i = 1 ; i < split.length ; i++) {
                                value.append(split[i]);
                            }
                            mail.setDestinataire(value.toString());
                            break;

                        case "subject":
                            value.setLength(0);
                            value = new StringBuilder();
                            for (int i = 1 ; i < split.length ; i++) {
                                value.append(split[i]);
                            }
                            mail.setSujet(value.toString());
                            break;

                        case "message-id":
                            value.setLength(0);
                            value = new StringBuilder();
                            for (int i = 1 ; i < split.length ; i++) {
                                value.append(split[i]);
                            }
                            mail.setMessageId(value.toString());
                            break;

                        default:
                            mail.setMessage(mail.getMessage().concat(reponse).concat("\r\n"));
                            break;
                    }
                }

            } while (!(reponse.equals(".") || reponse.equals("-ERR")));
            mail.saveInFile(this.client, Integer.parseInt(messageId));
            this.client.getMailObservableList().add(mail);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
