package Entities;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Client {
    // Attributs
    public static String END_LINE = "\r\n";

    private Socket socket;
    private String name;
    private String password;
    private String salt;
    private MessageDigest messageDigest;
    private BufferedOutputStream bufferedOutputStream;
    private BufferedReader bufferedReader;
    private ObservableList<Mail> mailObservableList;

    public Client() throws NoSuchAlgorithmException {
        messageDigest = MessageDigest.getInstance("MD5");
        mailObservableList = FXCollections.observableArrayList();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public BufferedReader getBufferedReader() {
        return bufferedReader;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public MessageDigest getMessageDigest() {
        return messageDigest;
    }

    public BufferedOutputStream getBufferedOutputStream() {
        return bufferedOutputStream;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(String adresseIp, int port) throws Exception {
        if (this.socket != null)
            throw new Exception("Le est déjà connecté");

        InetAddress server = InetAddress.getByName(adresseIp);
        this.socket = new Socket(server, port);
        this.bufferedOutputStream = new BufferedOutputStream(this.socket.getOutputStream());
        this.bufferedReader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
    }

    public ObservableList<Mail> getMailObservableList() {
        return mailObservableList;
    }

    public void closeSocket() {
        if (this.socket !=null) {
            this.bufferedOutputStream = null;
            this.bufferedReader = null;
            try {
                this.socket.close();
                this.socket = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
