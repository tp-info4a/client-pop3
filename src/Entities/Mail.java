package Entities;

import javafx.beans.property.SimpleStringProperty;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.time.LocalDate;

public class Mail {
    private String messageId;
    private SimpleStringProperty auteur;
    private SimpleStringProperty sujet;
    private SimpleStringProperty message;
    private LocalDate date;
    private SimpleStringProperty destinataire;

    public Mail() {
        this.auteur = new SimpleStringProperty("");
        this.sujet = new SimpleStringProperty("");
        this.message = new SimpleStringProperty("");
        this.date = LocalDate.now();
        this.destinataire = new SimpleStringProperty("");
    }

    public Mail(String auteur, String destinataire,  String sujet, String message, String date) {
        this.auteur = new SimpleStringProperty(auteur);
        this.sujet = new SimpleStringProperty(sujet);
        this.message = new SimpleStringProperty(message);
        String[] numbers = date.split("/");
        this.date = LocalDate.of(Integer.parseInt(numbers[2]), Integer.parseInt(numbers[1]), Integer.parseInt(numbers[0]));
        this.destinataire = new SimpleStringProperty(destinataire);
    }

    public String getAuteur() {
        return auteur.get();
    }

    public SimpleStringProperty auteurProperty() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur.set(auteur);
    }

    public String getSujet() {
        return sujet.get();
    }

    public SimpleStringProperty sujetProperty() {
        return sujet;
    }

    public void setSujet(String sujet) {
        this.sujet.set(sujet);
    }

    public String getMessage() {
        return message.get();
    }

    public SimpleStringProperty messageProperty() {
        return message;
    }

    public void setMessage(String message) {
        this.message.set(message);
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getDestinataire() {
        return destinataire.get();
    }

    public SimpleStringProperty destinataireProperty() {
        return destinataire;
    }

    public void setDestinataire(String destinataire) {
        this.destinataire.set(destinataire);
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public void saveInFile(Client client, int number) {
        try {
            File file = new File("./Resources/" + client.getName() + number + ".json");
            file.createNewFile();
            file.setWritable(true);
            PrintWriter out = new PrintWriter(new FileWriter(file));
            out.println(this.toString());
            out.close();
        } catch (Exception e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "{" +
                "\"messageId\":\"" + messageId + "\""+
                ", \"auteur\":\"" + auteur.getValue() + "\""+
                ", \"sujet\":\"" + sujet.getValue() + "\""+
                ", \"message\":\"" + message.getValue().replace("\r\n", " ") + "\""+
                ", \"date\":\"" + date.toString() + "\""+
                ", \"destinataire\":\"" + destinataire.getValue() + "\""+
                '}';
    }
}
