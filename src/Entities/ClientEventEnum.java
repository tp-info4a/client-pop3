package Entities;

public enum ClientEventEnum {
    CONNEXION_SERVEUR,
    CONNEXION_UTILISATEUR,
    QUIT,
    LIST
}
