import Controllers.ApplicationController;
import Entities.Client;
import Entities.ClientEventEnum;
import Services.ClientService;
import javafx.application.Application;
import javafx.stage.Stage;

import java.security.NoSuchAlgorithmException;

public class ApplicationClientMail extends Application {

    private Client client;
    private ClientService clientService;

    @Override
    public void start(Stage primaryStage) {
        try {
            this.client = new Client();
            this.clientService = new ClientService(this.client);

            ApplicationController applicationController = new ApplicationController(this.client, this.clientService);
            primaryStage.setTitle("Client Mail");

            applicationController.formIP(primaryStage);

            primaryStage.show();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop() {
        if (this.client.getSocket() != null)
            this.clientService.actionOnEvent(ClientEventEnum.QUIT, null);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
